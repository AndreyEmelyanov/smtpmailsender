package networklab4;

import java.io.File;
import java.io.IOException;
import javax.mail.*;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import java.util.Date;
import java.util.Properties;
import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Smtpmail extends Application
{
    @FXML
    private TextField FromTextBox;
    @FXML
    private TextField ToTextBox;
    @FXML
    private TextField SubjectTextBox;
    @FXML
    private TextField ServerTextBox;
    @FXML
    private TextField PortTextBox;
    @FXML
    private TextArea SendTextArea;
    @FXML
    private Button SendButton;

    @FXML
    private CheckBox AuthCheckBox;
    @FXML
    private PasswordField PaswordTextBox;
    @FXML
    private Button AttachmentButton;

    private Stage stage;
    @FXML
    private Label FileLAbel;

    private File file;

    @Override
    public void start(Stage primaryStage) throws IOException
    {
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("View.fxml"));
        primaryStage.setTitle("Сети. Почта SMTP.");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

    @FXML
    private void SendButtonMouseClicked(MouseEvent event) throws IOException
    {
        MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content- handler=com.sun.mail.handlers.message_rfc822");
        if (!ServerTextBox.getText().trim().equals("") && !ToTextBox.getText().trim().equals("") && !ServerTextBox.getText().trim().equals(""))
        {
            Session session;
            Properties props = new Properties();
            if (AuthCheckBox.isSelected())
            {
                Authenticator auth = new Authenticator()
                {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication(FromTextBox.getText().trim(), PaswordTextBox.getText().trim());
                    }
                };
                if (ServerTextBox.getText().trim().equals("smtp.gmail.com"))
                {
                    if (AuthCheckBox.isSelected() && (!AuthCheckBox.getText().trim().equals("")))
                    {
                        System.out.println("TLSEmail Start");
                        props = new Properties();
                        props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
                        props.put("mail.smtp.port", "587"); //TLS Port
                        props.put("mail.smtp.auth", "true"); //enable authentication
                        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
                        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
                    }
                    else
                    {
                        AuthCheckBox.setSelected(true);
                        PaswordTextBox.setVisible(true);
                        PaswordTextBox.setText("Для smtp.gmail.com необходима авторизация");
                    }
                }
                else
                {
                    props = new Properties();
                    props.put("mail.smtp.host", ServerTextBox.getText().trim()); //SMTP Host
                    props.put("mail.smtp.port", PortTextBox.getText().trim()); //TLS Port
                    props.put("mail.smtp.auth", "true");
                }
                session = Session.getInstance(props, auth);

            }
            else
            {
                props = new Properties();
                props.put("mail.smtp.host", ServerTextBox.getText().trim()); //SMTP Host
                props.put("mail.smtp.port", PortTextBox.getText().trim()); //TLS Port
                props.put("mail.smtp.auth", "false");
                session = Session.getInstance(props);
            }
            sendEmail(session, ToTextBox.getText().trim(), SubjectTextBox.getText().trim(), SendTextArea.getText());
        }
    }

    public void sendEmail(Session session, String toEmail, String subject, String body)
    {
        try
        {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            MimeMessage msg = new MimeMessage(session);
            //set message headers

            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(FromTextBox.getText().trim()));

            msg.setSubject(subject, "UTF-8");

            msg.setSentDate(new Date());

            String[] str = toEmail.split(",");
            for (String el : str)
                msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(el, false));

            if (file != null)
            {
                BodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setText(body);
                Multipart multipart = new MimeMultipart();
                multipart.addBodyPart(messageBodyPart);
                messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(file);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(file.getName());
                multipart.addBodyPart(messageBodyPart);
                msg.setContent(multipart);
            }
            else
            {
                msg.setText(body, "UTF-8");
            }
            System.out.println("Message is ready");

            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            Transport.send(msg);

            System.out.println("EMail Sent Successfully!!");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @FXML
    private void AuthCheckBoxAction(ActionEvent event)
    {
        if (AuthCheckBox.isSelected())
            PaswordTextBox.setVisible(true);
        else
            PaswordTextBox.setVisible(false);
    }

    @FXML
    private void AttachmentButtonClicked(ActionEvent event) throws MessagingException
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите файл для добавления");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Все Файлы", "*.*"));
        file = fileChooser.showOpenDialog(stage);
        FileLAbel.setVisible(true);
    }
}
